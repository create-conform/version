/////////////////////////////////////////////////////////////////////////////////////////////
//
// version
//
//    Library for processing semantic version strings.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Version class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Version() {
    var self = this;

    this.UPGRADABLE_NONE = null;
    this.UPGRADABLE_PATCH = "patch";
    this.UPGRADABLE_MINOR = "minor";
    this.UPGRADABLE_MAJOR = "major";

    this.find = function(collection, search, opt_upgradable) {
        var sortedCollection = self.sort(collection, "desc");
        for (var p in sortedCollection) {
            if (self.compare(sortedCollection[p], search, opt_upgradable)) {
                return collection[sortedCollection[p]];
            }
        }
    };
    this.sort = function(collection, order) {
        var props = [];
        for (var p in collection) {
            props.push(p);
        }

        var sort = {
            asc: function (a, b) {
                var reA = /[^a-zA-Z]/g;
                var reN = /[^0-9]/g;
                for (var l=0;l < Math.min(a.value.length, b.value.length); l++) {
                    if (a.value[l] === b.value[l]) {
                        continue;
                    }
                    // put numbers before strings
                    if (!isNaN(a.value[l]) && isNaN(b.value[l])) {
                        return 1;
                    }
                    if (isNaN(a.value[l]) && !isNaN(b.value[l])) {
                        return -1;
                    }
                    // compare
                    var aA = a.value[l].replace(reA, "");
                    var bA = b.value[l].replace(reA, "");
                    if(aA === bA) {
                        var aN = parseInt(a.value[l].replace(reN, ""), 10);
                        var bN = parseInt(b.value[l].replace(reN, ""), 10);
                        //return aN === bN ? 0 : aN > bN ? 1 : -1;
                        if (aN === bN) {
                            continue;
                        }
                        return aN > bN ? 1 : -1;
                    } else {
                        return aA > bA ? 1 : -1;
                    }
                }
                return 0;
            },
            desc: function (a, b) {
                return sort.asc(b, a);
            }
        };

        var mapped = props.map(function (el, i) {
            return { index: i, value: el.split(/[\.\/]+/), name : el };
        });

        mapped.sort(sort[order] || sort.asc);

        return mapped.map(function (el) {
            return el.name;
        });
    };
    this.compare = function(str, search, opt_upgradable) {
        if (!str) {
            return;
        }
        if (!search) {
            return true;
        }
        var strRes = str.split("/");
        str = strRes[0];
        strRes = strRes.join("/").substr(str.length);
        var searchRes = search.split("/");
        search = searchRes[0];
        searchRes = searchRes.join("/").substr(search.length);
        var groups = 0;
        switch (opt_upgradable) {
            case "patch":
                groups = 1;
                break;
            case "minor":
                groups = 2;
                break;
            case "major":
                groups = 3;
                break;
        }
        //if (groups === 0) {
        //    return str == search;
        //}
        var parts = search.split(".");
        var numbers = 0;
        for (var p = parts.length; p > 0; p--) {
            if (isNaN(parts[p - 1])) {
                break;
            }
            numbers++;
        }
        var match = str.split(".");
        // if parts are not equal, they can't match
        if (match.length != (parts.length - numbers) + 3 || strRes != searchRes) {
            return false;
        }
        // check if id string without number parts match
        for (var p = 0; p < parts.length - numbers; p++) {
            if (match.length < p || match[p] != parts[p]) {
                return false;
            }
        }
        // check the number parts
        for (var g = 0; g < 3; g++) {
            if (g < 3 - numbers) {
                continue;
            }
            if (g < groups && match[match.length - 1 - g] < parts[parts.length - 1 - (g - (3 - numbers))]) {
                return false;
            }
            else if(g >= groups && match[match.length - 1 - g] != parts[parts.length - 1 - (g - (3 - numbers))]) {
                return false;
            }
        }

        return true;
    };
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = new Version();